//
//  ViewController.h
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroView : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *picker2;

@property (weak, nonatomic) IBOutlet UIPickerView *picker;
//- (void) switchScreen;
@end

