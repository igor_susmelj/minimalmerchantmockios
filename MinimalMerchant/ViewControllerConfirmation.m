//
//  ViewControllerConfirmation.m
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import "ViewControllerConfirmation.h"

@interface ViewControllerConfirmation ()

@end

@implementation ViewControllerConfirmation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"Now reached confirmation screen");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
