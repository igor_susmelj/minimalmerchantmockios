//
//  ViewController.m
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import "IntroView.h"
#import "AppDelegate.h"
@import SIXMerchantSDK;

@interface IntroView ()
{
    NSArray * pickerData;
}

@property (strong, nonatomic) SIXPaymentAppOffer * presentedOffer;

@end

@implementation IntroView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //initialize picker
    pickerData = @[@"0", @"1", @"2", @"3", @"4", @"5"];
    self.picker.dataSource = self;
    self.picker.delegate  = self;
    
    self.picker2.dataSource = self;
    self.picker2.delegate = self;
}

- (IBAction)orderNowClicked:(id)sender {
    
    
    //hacky way to get access to the appDelegate
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    
    SIXPayment * payment =
    [[SIXPayment alloc] initWithMerchantOrderIdentifier:@"test"
                                                  price:  [[SIXAmount alloc]
                                                           initWithValue: [NSNumber numberWithFloat: 10]
                                                           currencyCode:@"CHF"]
                                            userTimeout:120.0
                                        showReceiptTime:1.0];
    [appDelegate.paymentGateway beginPayment:payment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}

@end
