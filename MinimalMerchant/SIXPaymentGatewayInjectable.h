//
//  SIXPaymentGatewayInjectable.h
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIXPaymentGateway;


@protocol SIXPaymentGatewayInjectable <NSObject>

- (void)setPaymentGateway:(SIXPaymentGateway *)gateway;

@end
