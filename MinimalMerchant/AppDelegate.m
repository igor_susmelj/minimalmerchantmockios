//
//  AppDelegate.m
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import "AppDelegate.h"
#import "IntroView.h"
//#import "SIXPaymentGatewayInjectable.h"

@import SIXMerchantSDK;


@interface AppDelegate () <SIXPaymentGatewayDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.paymentGateway = [[SIXPaymentGateway alloc] initWithMerchantAlias:@"1234"
                                                               merchantKey:@"U0pjSr+E9ZLGtb/OnaiZYA=="
                                                        merchantKeyVersion:@"01"
                                                                 returnURL:@"minimalmerchant"];
    
    self.paymentGateway.delegate = self;
//    (id<SIXPaymentGatewayInjectable>)self.intro setPaymentGateway:self.paymentGateway];
    [self.paymentGateway addPaymentObserver:self];
    
    
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(nonnull NSURL *)url
{
    return [self.paymentGateway handleTransactionCallbackWithURL:url];
}

- (void)paymentGateway:(SIXPaymentGateway *)gateway didFinishProcessingTransaction: (SIXPaymentTransaction *)transaction
{
    BOOL isPaymentSuccessfull = transaction.result == SIXPaymentTransactionResultApproved;
    
    if(!isPaymentSuccessfull){
        NSLog(@"Payment failed. Error %@(%04ld)", transaction.error.localizedDescription, (long)transaction.error.code);
        self.msgBackSwitch = @"failure";
    }else{
        
        NSLog(@"Payment successful, \nTransaction ID: %@\nTransaction Date Time: %@", transaction.paymentConfirmation.transactionIdentifier, transaction.paymentConfirmation.transactionDateTime);
        self.msgBackSwitch = @"success";
        
        //change root view to confirmation screen
        self.window.rootViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationView"];

    }
    
}

- (void)paymentGateway:(SIXPaymentGateway *)gateway didReceivePaymentAppOffer:(SIXPaymentAppOffer *)offer
{
    // MARK: Handle payment app selection within "first payment" flow
//    [self.window.appRootViewController presentPaymentAppSelectionInterfaceWithOffer:offer];
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



@end
