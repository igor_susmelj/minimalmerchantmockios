//
//  AppDelegate.h
//  MinimalMerchant
//
//  Created by Igor Susmelj on 10/02/16.
//  Copyright © 2016 Igor Susmelj. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SIXMerchantSDK;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *msgBackSwitch;
@property (strong, nonatomic) SIXPaymentGateway * paymentGateway;


@end

