# README #

This repository describes a minimal implementation of the Paymit app switch API using the SIXMerchantSDK.

#Short description#

The following settings to the project have been made:

* Deployment target: `8.0`
* Added the SIXMerchantSDK.framework
* Added the bash script mentioned in the implementation guide to the run scripts
* Added the URL scheme

The application does the following:

* Initialises the payment gateway in `AppDelegate.m`
    * Registers the callback URL scheme for the Paymit app
    * Handles the URL parameters if callback from Paymit app arises
        * Sets root view controller to the `Confirmation` view if Payment was successful
* Handles the pay button in `IntroView.m`
    * Initialises the payment

Note that this mock application **is not making a valid payment** and is **not handling errors** which could arise during an app switch!