//
//  SIXPaymentAppSelectionViewController.h
//  SIXMerchantSDK
//
//  Created by Piotr Wegrzynek on 14/12/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SIXPaymentAppSelectionViewController, SIXPaymentAppRecord, SIXPaymentAppSelection;


NS_ASSUME_NONNULL_BEGIN


/**
 * Payment app selection UI view controller delegate protocol
 */
@protocol SIXPaymentAppSelectionViewControllerDelegate <NSObject>

/**
 * Notifies the delegate that user has made a selection.
 *
 * @param viewController notifying view controller
 * @param selection selection made by the user
 */
- (void)paymentAppSelectionViewController:(SIXPaymentAppSelectionViewController *)viewController
                         didMakeSelection:(SIXPaymentAppSelection *)selection;

/**
 * Notifies the delegate that user has aborted selection.
 *
 * @param viewController notifying view controller
 */
- (void)paymentAppSelectionViewControllerDidCancel:(SIXPaymentAppSelectionViewController *)viewController;

/**
 * Invoked when user requested to navigate to a webpage with download links for all available issuer apps.
 *
 * @param viewController notifying view controller
 * @return YES to allow web browser to be opened on download links webpage, NO to prevent it
 */
- (BOOL)paymentAppSelectionViewControllerShouldRedirectToApplicationCatalog:(SIXPaymentAppSelectionViewController *)viewController;

@end


/**
 * The default implementation of payment app selection UI.
 */
@interface SIXPaymentAppSelectionViewController : UITableViewController

/// View controller's delegate.
@property (weak, nonatomic) id<SIXPaymentAppSelectionViewControllerDelegate> delegate;

/**
 * Initializes the receiver to display a list of available issuer apps.
 *
 * @param availableApps records to be displayed on the list
 * @return initialized instance
 */
- (instancetype)initWithAvailablePaymentApps:(NSArray<SIXPaymentAppRecord *> *)availableApps NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithStyle:(UITableViewStyle)style NS_UNAVAILABLE;
- (instancetype)initWithNibName:(nullable NSString *)nibNameOrNil bundle:(nullable NSBundle *)nibBundleOrNil NS_UNAVAILABLE;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@end


NS_ASSUME_NONNULL_END
