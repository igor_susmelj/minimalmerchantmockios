//
//  SIXPaymentSettings.h
//  SIXMerchantSDK
//
//  Created by Piotr Wegrzynek on 11/12/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@class SIXPaymentSettings, SIXPaymentAppRecord;


NS_ASSUME_NONNULL_BEGIN


/**
 * Manages issuer app selection.
 *
 * NOTE: On iOS 9, it is important to obey URL scheme querying rules.
 * https://developer.apple.com/library/ios/documentation/General/Reference/InfoPlistKeyReference/Articles/LaunchServicesKeys.html#//apple_ref/doc/uid/TP40009250-SW14
 */
@interface SIXPaymentSettings : NSObject

/// Currently selected URL scheme name.
@property (copy, nonatomic, readonly, nullable) NSString * selectedURLScheme;

/**
 * Tells whether any issuer apps have been detected on device.
 *
 * @return YES if at least app has been detected, NO otherwise
 */
+ (BOOL)isAnyPaymentAppInstalled;

/**
 * Resets the persisted URL scheme selection.
 * Will not affect selectedURLScheme on existing instances.
 */
+ (void)resetPersistentURLSchemeSelection;

@end


@interface UIApplication (SIXPaymentSettings)

/**
 * Opens web browser on a page that lists available issuer apps.
 *
 * @return YES if browser was opened successfully, NO otherwise
 */
- (BOOL)openPaymentApplicationCatalogURL;

@end


NS_ASSUME_NONNULL_END
