//
//  SIXMerchantSignature.h
//  SIXMerchantSDK
//
//  Created by Piotr Wegrzynek on 25/01/16.
//  Copyright © 2016 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIXPayment;


/**
 * Represents the signature authorizing a payment request.
 */
@interface SIXMerchantSignature : NSObject

/// HMAC bytes
@property (copy, nonatomic, readonly) NSData * hmac;

/// Signature algorithm version identifier.
@property (copy, nonatomic, readonly) NSString * signatureVersion;

/// Signer's key version identifier.
@property (copy, nonatomic, readonly) NSString * keyVersion;

/**
 * Initializes the receiver.
 *
 * @param hmac SHA256 HMAC bytes
 * @param signatureVersion signature algorithm version identifier (2 decimal digits)
 * @param keyVersion signer's key version identifier (2 decimal digits)
 * @return initialized instance
 */
- (instancetype)initWithHmac:(NSData *)hmac
            signatureVersion:(NSString *)signatureVersion
                  keyVersion:(NSString *)keyVersion NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

@end


/**
 * Represents the set of payment request parameters over which a merchant's signature is calculated.
 * Does not include parameters related to signature generation itself (signature algorithm version, key version).
 */
@interface SIXMerchantSignatureInput : NSObject

/// Transferred payment.
@property (strong, nonatomic, readonly) SIXPayment * payment;

/// Alias of the merchant that requests payment.
@property (copy, nonatomic, readonly) NSString * merchantAlias;

/// Callback URL scheme to send payment response.
@property (copy, nonatomic, readonly) NSString * returnURL;

/// Requesting merchant SDK version identifier.
@property (copy, nonatomic, readonly) NSString * merchantSdkVersion;

/// Version identifier for acquirer's public key that SDK will use to locally verify the receipt signature.
@property (copy, nonatomic, readonly) NSString * acquirerKeyVersion;

- (instancetype)init NS_UNAVAILABLE;

@end
