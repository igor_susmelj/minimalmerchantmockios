//
//  SIXPaymentGateway.h
//  MerchantDemo
//
//  Created by Piotr Wegrzynek on 17/11/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIXPaymentGateway, SIXPaymentTransaction, SIXPayment, SIXPaymentAppOffer, SIXMerchantSignature, SIXMerchantSignatureInput;


NS_ASSUME_NONNULL_BEGIN


/**
 * A protocol used to notify about payment transaction results.
 */
@protocol SIXPaymentObserver <NSObject>

@optional

/**
 * After request is prepared, asks the observer whether to continue, switching to issuer app.
 * Only proceeds if all observers that implement this method return YES.
 *
 * @param gateway instance that is about to invoke issuer app
 * @param payment payment that will be included in the request to issuer app
 */
- (BOOL)paymentGateway:(SIXPaymentGateway *)gateway shouldInitializeTransactionForPayment:(SIXPayment *)payment;

/**
 * Notifies the observer that a payment request could not be delivered to issuer app.
 *
 * @param gateway instance that failed to invoke issuer app
 * @param payment payment that was included in the request that failed to reach issuer app
 * @param error error that was encountered when trying to invoke issuer app
 */
- (void)paymentGateway:(SIXPaymentGateway *)gateway didFailToInitializeTransactionForPayment:(SIXPayment *)payment withError:(NSError *)error;

/**
 * Notifies the observer that a response has been received from issuer app
 * and processing is considered finished for given transaction.
 *
 * @param gateway instance that received the callback from payment app
 * @param transation the processed transaction
 */
- (void)paymentGateway:(SIXPaymentGateway *)gateway didFinishProcessingTransaction:(SIXPaymentTransaction *)transaction;

@end


/**
 * A protocol containing gateway callbacks that are not strictly related to the actual payment process.
 */
@protocol SIXPaymentGatewayDelegate <NSObject>

/**
 * Notifies the delegate that issuer application has to be selected before payment can continue.
 * This happens when user hasn't selected any issuer application yet or if new applications have
 * been detected on the device.
 * If the delegate is not set, payment transaction initialization will fail in such scenario.
 *
 * @param gateway instance that requests issuer app selection
 * @param offer an offer listing available issuer apps
 * NOTE: It is important to always invoke either -applySelection: or -abortSelection on the passed offer,
 * otherwise payment process will stall.
 */
- (void)paymentGateway:(SIXPaymentGateway *)gateway didReceivePaymentAppOffer:(SIXPaymentAppOffer *)offer;

@optional

/**
 * Allows the delegate to provide an externally-calculated payment request signature (HMAC).
 * If this callback is not implemented, the gateway will compute the signature using
 * the key provided at initialization time. This callback must be implemented and the delegate
 * must be set before attempting payments if the key has not been provided during initialization.
 *
 * @param gateway instance that requests a signature
 * @param input input over which the signature is to be generated
 * @param completionHandler Block to be invoked to deliver the generated signature.
 * NOTE: It is important to always invoke this handler, otherwise payment process will stall.
 * NOTE: If the signature is obtained on a background thread, it is important to dispatch to
 * the main thread before invoking this handler.
 */
- (void)paymentGateway:(SIXPaymentGateway *)gateway
     signatureForInput:(SIXMerchantSignatureInput *)input
     completionHandler:(void (^)(SIXMerchantSignature * signature))completionHanlder;

/**
 * Upon receiving successful payment confirmation acquirer's signature validation is required.
 * The delegate can override evaluation logic by implementing this callback.
 * After performing its own evaluation, the delegate is expected to call completion handler
 * with the result.
 *
 * @param gateway instance that requests signature evaluation
 * @param transaction transaction for which payment confirmation has been received
 * @param completionHandler Handler to be invoked to deliver custom signature validation results.
 * Passing NO makes the payment fail with "Invalid receipt signature" error (0010).
 * NOTE: It is important to always invoke this handler, otherwise payment process will stall.
 * NOTE: If evaluation is performed on a background thread, it is important to dispatch to
 * the main thread before invoking this handler.
 */
- (void)paymentGateway:(SIXPaymentGateway *)gateway
didReceiveAcquirerSignatureForTransaction:(SIXPaymentTransaction *)transaction
     completionHandler:(void (^)(BOOL isSignatureAccepted))completionHandler;

@end


/**
 * Entry point for handling payments on merchant's app side.
 */
@interface SIXPaymentGateway : NSObject

/// Receiver's delegate object.
@property (weak, nonatomic) id<SIXPaymentGatewayDelegate> delegate;

/**
 * Initializes the receiver.
 *
 * @param merchantAlias alias identifying the merchant registered in SIX database
 * @param merchantKey Secret key used to authorise payment requests received from SIX.
 * If this is not provided, a delegate that implements -paymentGateway:signatureForInput:completionHandler:
 * must be set prior to attempting any payments.
 * @param merchantKeyVersion Key version identifier provided by SIX.
 * If this is not provided, a delegate that implements -paymentGateway:signatureForInput:completionHandler:
 * must be set prior to attempting any payments.
 * @param returnURL URL scheme that will be used by payment app to send transaction responses
 * @return initialized instance
 */
- (instancetype)initWithMerchantAlias:(NSString *)merchantAlias
                          merchantKey:(nullable NSString *)merchantKey
                   merchantKeyVersion:(nullable NSString *)merchantKeyVersion
                            returnURL:(NSString *)returnURL NS_DESIGNATED_INITIALIZER;

/**
 * Convenience initializer that omits parameters related to merchant signature generation.
 * Gateways initialized with this must have a delegate that implements
 * -paymentGateway:signatureForInput:completionHandler: set prior to attempting any payments.
 *
 * @param merchantAlias alias identifying the merchant registered in SIX database
 * @param returnURL URL scheme that will be used by payment app to send transaction responses
 * @return initialized instance
 */
- (instancetype)initWithMerchantAlias:(NSString *)merchantAlias
                            returnURL:(NSString *)returnURL;
- (instancetype)init NS_UNAVAILABLE;

/**
 * Registers an observer to be notified of payment transaction outcome.
 *
 * @param observer observer to be registered
 */
- (void)addPaymentObserver:(id<SIXPaymentObserver>)observer;

/**
 * Begins payment flow for given payment.
 *
 * @param payment payment to initiate
 */
- (void)beginPayment:(SIXPayment *)payment;

/**
 * Processes incoming payment callback URL, notifying registered observers.
 * Note:
 * Only callbacks for payments initiated with beginPayment: by the same instance can be handled.
 *
 * @param incomingURL URL received in application delegate's -application:handleOpenURL:
 * @return YES if callback was handled successfully, NO otherwise
 */
- (BOOL)handleTransactionCallbackWithURL:(NSURL *)incomingURL;

@end




NS_ASSUME_NONNULL_END
