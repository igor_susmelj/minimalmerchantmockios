//
//  SIXPaymentAppOffer.h
//  SIXMerchantSDK
//
//  Created by Piotr Wegrzynek on 03/02/16.
//  Copyright © 2016 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@class SIXPaymentAppRecord, SIXPaymentAppSelection;


NS_ASSUME_NONNULL_BEGIN


/**
 * An offer consists of a list of issuer apps available on the device.
 * Payment gateway presents an offer to its delegate when app selection is required.
 */
@interface SIXPaymentAppOffer : NSObject

/// A list of issuer apps to choose from
@property (copy, nonatomic, readonly) NSArray<SIXPaymentAppRecord *> * availableApps;

- (instancetype)init NS_UNAVAILABLE;

/**
 * Selects an issuer app to be used to handle a payment.
 *
 * @param selection The selection to apply. It must specify an app record from the availableApps array.
 */
- (void)applySelection:(SIXPaymentAppSelection *)selection;

/**
 * Declines the offer represented by receiver.
 * Calling this will cause associated payment to fail.
 */
- (void)abortSelection;

@end


/**
 * Payment app selection consists of the chosen app and the decision
 * whether this app is to be used for all future payments.
 */
@interface SIXPaymentAppSelection : NSObject

/// Selected payment app.
@property (strong, nonatomic, readonly) SIXPaymentAppRecord * selectedAppRecord;

/**
 * Flag defining selection persistence.
 * Selection should be stored for future payments if YES and should only be used for single payment otherwise.
 */
@property (assign, nonatomic, readonly, getter=isPersistent) BOOL persistent;

/**
 * Initializes the receiver.
 *
 * @param selectedAppRecord selected payment app
 * @param isPersistent flag defining selection persistence
 * @return initialized instance
 */
- (instancetype)initWithSelectedAppRecord:(SIXPaymentAppRecord *)selectedAppRecord
                          persistenceFlag:(BOOL)isPersistent NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

@end


/**
 * Issuer app metadata.
 */
@interface SIXPaymentAppRecord : NSObject

/// Application name.
@property (copy, nonatomic, readonly) NSString * name;

/// Application icon image data.
@property (copy, nonatomic, readonly) NSData * iconImageData;

/// Original application icon image size.
@property (assign, nonatomic, readonly) CGSize originalIconImageSize;

/// URL scheme under which the application accepts payment requests.
@property (copy, nonatomic, readonly) NSString * URLScheme;

- (instancetype)init NS_UNAVAILABLE;

@end


NS_ASSUME_NONNULL_END
