//
//  SIXPaymentTransaction.h
//  MerchantDemo
//
//  Created by Piotr Wegrzynek on 17/11/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIXPayment, SIXPaymentRequest, SIXPaymentConfirmation, SIXReceiptSignature;


NS_ASSUME_NONNULL_BEGIN

/**
 * Possible SIXPaymentTransaction outcome types.
 */
typedef NS_ENUM(NSInteger, SIXPaymentTransactionResult) {
    SIXPaymentTransactionResultUnknown,    // transaction is not finalized yet
    SIXPaymentTransactionResultApproved,   // transaction completed successfully
    SIXPaymentTransactionResultDeclined,   // transaction declined, check error property
    SIXPaymentTransactionResultFailed      // transaction failed for unexpected reasons, check error property
};


/**
 * Represents outcome of a payment.
 * Contains confirmation details for successful payments and error information for failed ones.
 */
@interface SIXPaymentTransaction : NSObject

/// The subject of transaction.
@property (strong, nonatomic, readonly) SIXPayment * payment;

/// General outcome.
@property (assign, nonatomic, readonly) SIXPaymentTransactionResult result;

/// Payment confirmation. Available only for approved transactions.
@property (strong, nonatomic, readonly, nullable) SIXPaymentConfirmation * paymentConfirmation;

/// Transaction error. Available only for declined/failed transactions.
@property (strong, nonatomic, readonly, nullable) NSError * error;

- (instancetype)init NS_UNAVAILABLE;

@end


/**
 * Payment approval details.
 */
@interface SIXPaymentConfirmation : NSObject

/// Transaction identifier.
@property (copy, nonatomic, readonly) NSString * transactionIdentifier;

/// Transaction approval timestamp.
@property (strong, nonatomic, readonly) NSDate * transactionDateTime;

/// Locally verified payment receipt signature.
@property (strong, nonatomic, readonly) SIXReceiptSignature * acquirerSignature;

- (instancetype)init NS_UNAVAILABLE;

@end


/**
 * Represents the payment backend signature that confirms transaction.
 */
@interface SIXReceiptSignature : NSObject

/// Signature data, can be used for online verification.
@property (copy, nonatomic, readonly) NSData * signatureData;

/// Version of signer's key used to generate the signature.
@property (copy, nonatomic, readonly) NSString * keyVersion;

- (instancetype)init NS_UNAVAILABLE;

@end


NS_ASSUME_NONNULL_END
