//
//  SIXPayment.h
//  MerchantDemo
//
//  Created by Piotr Wegrzynek on 17/11/15.
//  Copyright © 2015 Trifork GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIXAmount;


NS_ASSUME_NONNULL_BEGIN


/**
 * Represents payment handled by SIX.
 */
@interface SIXPayment : NSObject

/// Unique order identifier assigned by merchant. Payments with the same identifier and price made within short timeframe will be recognized as retry attempts and no duplicate transfers will be performed.
@property (copy, nonatomic, readonly) NSString * merchantOrderIdentifier;

/// The amount to be transferred.
@property (strong, nonatomic, readonly) SIXAmount * price;

/// Overall duration after which initiated but unfinalized payment expires.
@property (assign, nonatomic, readonly) NSTimeInterval userTimeout;

/// Receipt presentation duration in payment app before switching back.
@property (assign, nonatomic, readonly) NSTimeInterval showReceiptTime;

/**
 * Initializes the receiver.
 *
 * @param merchantOrderIdentifier unique order identifier (1-50 ASCII characters)
 * @param price the amount to be transferred
 * @param userTimeout overall duration after which initiated but unfinalized payment expires
 * @param showReceiptTime receipt presentation duration in payment app before switching back
 * @return initialized instance
 */
- (instancetype)initWithMerchantOrderIdentifier:(NSString *)merchantOrderIdentifier
                                          price:(SIXAmount *)price
                                    userTimeout:(NSTimeInterval)userTimeout
                                showReceiptTime:(NSTimeInterval)showReceiptTime NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

@end


/**
 * Defines amount as currency along with monetary value.
 */
@interface SIXAmount : NSObject

/// Monetary value.
@property (strong, nonatomic, readonly) NSNumber * monetaryValue;

/// ISO 4217 code for currency in which the monetary value is expressed.
@property (copy, nonatomic, readonly) NSString * currencyCode;

/**
 * Initializes the receiver.
 *
 * @param value monetary value. For larger amounts, using NSDecimalNumber is suggested.
 * @param currencyCode ISO 4217 currency code. Currently only Swiss francs are supported.
 * @return initialized instance
 */
- (instancetype)initWithValue:(NSNumber *)value currencyCode:(NSString *)currencyCode NS_DESIGNATED_INITIALIZER;
- (instancetype)init NS_UNAVAILABLE;

@end


NS_ASSUME_NONNULL_END
