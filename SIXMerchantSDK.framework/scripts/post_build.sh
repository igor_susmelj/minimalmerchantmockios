#!/bin/sh

#  six_framework_install.sh
#  SIXMerchantSDK
#
#  Created by Piotr Wegrzynek on 02/02/16.
#  Copyright © 2016 Trifork GmbH. All rights reserved.

plist_dst="${BUILT_PRODUCTS_DIR}/${INFOPLIST_PATH}"
framework_root="${TARGET_BUILD_DIR}/${FRAMEWORKS_FOLDER_PATH}/SIXMerchantSDK.framework"
framework_file="${framework_root}/SIXMerchantSDK"
bc_symbol_maps_dir="${framework_root}/BCSymbolMaps"
scripts_dir="${framework_root}/scripts"

#########################################
# Add LSApplicationQueriesSchemes entries

echo "Adding SIX queried URL schemes to Info.plist at $plist_dst"

/usr/libexec/PlistBuddy -c "Add :LSApplicationQueriesSchemes array" "${plist_dst}"

for i in {40..1}
do
  scheme=$(printf "%s%03d" "paymit" $i)
  xpath="count(/plist/dict/key[.=\"LSApplicationQueriesSchemes\"]/following-sibling::array[1]/*[.=\"$scheme\"])"
  if !(( $(xmllint --xpath $xpath ${plist_dst}) )); then
    /usr/libexec/PlistBuddy -c "Add :LSApplicationQueriesSchemes:0 string $scheme" "${plist_dst}"
  fi
done

################################################################
# Strip irrelevant architecture slices from framework executable

archs="$(lipo -info "${framework_file}" | rev | cut -d ':' -f1 | rev)"

for arch in $archs; do
  if ! [[ "${VALID_ARCHS}" == *"$arch"* ]]; then
    echo "Stripping architecture $arch from executable $framework_file"
    lipo -remove "$arch" -output "${framework_file}" "${framework_file}" || exit 1
  fi
done

#################################################################################
# Move bitcode symbol maps from framework bundle to the top level build directory

if [ "$ACTION" == "install" ]; then
  echo "Copying bitcode symbol map files to $CONFIGURATION_BUILD_DIR"
  find "${bc_symbol_maps_dir}" -name '*.bcsymbolmap' -type f -exec cp {} "${CONFIGURATION_BUILD_DIR}" \; || exit 1
fi

#########################################################
# Purge build-time framework resources from target bundle

if [ "$ACTION" == "install" ]; then
  echo "Purging resources at $framework_root"
  rm -rf "${bc_symbol_maps_dir}" "${scripts_dir}"
fi

#############################################################################
# Re-sign framework since some resources/code slices might have been modified

if [ "${CODE_SIGNING_REQUIRED}" == "YES" ]; then
  echo "Code Signing ${framework_file} with Identity ${EXPANDED_CODE_SIGN_IDENTITY_NAME}"
  /usr/bin/codesign --force --sign ${EXPANDED_CODE_SIGN_IDENTITY} --preserve-metadata=identifier,entitlements "${framework_file}" || exit 1
fi
